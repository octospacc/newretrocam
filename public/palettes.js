/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * maple "mavica" syrup <maple@maple.pet> wrote this code.
 * As long as you retain this notice, you can do whatever you
 * want with this stuff. If we meet someday, and you think this
 * stuff is worth it, you can buy me a beer in return.
 * ------------------------------------------------------------
 */

// 4-color GB palette must be dark to light

const palettes = [
	[ // AYY4 - https://lospec.com/palette-list/ayy4
		[0, 48, 59],
		[255, 119, 119],
		[255, 206, 150],
		[241, 242, 218]
	],

	[ // SpaceHaze - https://lospec.com/palette-list/spacehaze
		[11, 6, 48],
		[107, 31, 177],
		[204, 52, 149],
		[248, 227, 196]
	],

	[ // CRTGB - https://lospec.com/palette-list/crtgb
		[6, 6, 1],
		[11, 62, 8],
		[72, 154, 13],
		[218, 242, 34]
	],

	[ // Amber CRTGB - https://lospec.com/palette-list/amber-crtgb
		[13, 4, 5],
		[94, 18, 16],
		[211, 86, 0],
		[254, 208, 24]
	],

	[ // Kirby (SGB) - https://lospec.com/palette-list/kirby-sgb
		[44, 44, 150],
		[119, 51, 231],
		[231, 134, 134],
		[247, 190, 247]
	],

	[ // CherryMelon - https://lospec.com/palette-list/cherrymelon
		[1, 40, 36],
		[38, 89, 53],
		[255, 77, 109],
		[252, 222, 234]
	],

	[ // Pumpkin GB - https://lospec.com/palette-list/pumpkin-gb
		[20, 43, 35],
		[25, 105, 44],
		[244, 110, 22],
		[247, 219, 126]
	],

	[ // Purpledawn - https://lospec.com/palette-list/purpledawn
		[0, 27, 46],
		[45, 117, 126],
		[154, 123, 188],
		[238, 253, 237]
	],

	[ // Royal4 - https://lospec.com/palette-list/royal4
		[82, 18, 150],
		[138, 31, 172],
		[212, 134, 74],
		[235, 219, 94]
	],

	[ // Grand Dad 4 - https://lospec.com/palette-list/grand-dad-4
		[76, 28, 45],
		[210, 60, 78],
		[95, 177, 245],
		[234, 245, 250]
	],

	[ // Mural GB - https://lospec.com/palette-list/mural-gb
		[10, 22, 78],
		[162, 81, 48],
		[206, 173, 107],
		[250, 253, 255]
	],

	[ // Ocean GB - https://lospec.com/palette-list/ocean-gb
		[28, 21, 48],
		[42, 48, 139],
		[54, 125, 1216],
		[141, 226, 246]
	],

	[ // Alleyway - ISS
		[66, 66, 66],
		[123, 123, 206],
		[255, 107, 255],
		[255, 214, 0]
	],

	[ // Pocket - ISS
		[108, 108, 78],
		[142, 139, 97],
		[195, 196, 165],
		[227, 230, 201]
	],

	[ // Kadabura4 - https://lospec.com/palette-list/kadabura4
		[0, 0, 0],
		[87, 87, 87],
		[219, 0, 12],
		[255, 255, 255]
	],

	[ // Virtual - ISS
		[2, 0, 0],
		[65, 0, 0],
		[127, 0, 0],
		[255, 0, 0]
	],

	[ // Love! Love! - ISS
		[176, 16, 48],
		[255, 96, 176],
		[255, 184, 232],
		[255, 255, 255]
	],

	[ // Metroid II (SGB) - https://lospec.com/palette-list/metroid-ii-sgb
		[44, 23, 0],
		[4, 126, 96],
		[182, 37, 88],
		[174, 223, 30]
	],

	[ // Micro 86 - https://lospec.com/palette-list/micro-86
		[38, 0, 14],
		[255, 0, 0],
		[255, 123, 48],
		[255, 217, 178]
	],

	[ // Vivid 2Bit Scream - https://lospec.com/palette-list/vivid-2bit-scream
		[86, 29, 23],
		[92, 79, 163],
		[116, 175, 52],
		[202, 245, 50]
	],

	[ // Pastel GBC/SGB - submitted by synth___ruiner
		[4,2,4],
		[156,146,244],
		[236,138,140],
		[252,250,172]
	],

	[ // trans flag - by mavica
		[32, 32, 32],
		[91, 207, 250],
		[245, 171, 185],
		[255, 255, 255]
	],

	[ // grayscale - by mavica
		[40, 40, 40],
		[104, 104, 104],
		[168, 168, 168],
		[252, 252, 252]
	],

	[ // Scold 2 bit - https://lospec.com/palette-list/scold-2-bit
		[16, 28, 86],
		[206, 0, 148],
		[15, 183, 0],
		[211,211,211]
	],

	[ // strawberry parfait - by mavica
		[31, 19, 0],
		[216, 32, 46],
		[247, 80, 215],
		[255, 231, 204]
	],

	[ // bric-a-brac - by mavica
		[12, 39, 56],
		[237, 79, 54],
		[248, 150, 23],
		[184, 211, 218]
	]
];