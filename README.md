# NewRetroCam
Retro GameBoy Camera experience with a New twist!

[✅ Try it here](https://octospacc.gitlab.io/newretrocam)

## Info
This app is a web-based camera that replicates the style and charm of the GameBoy Camera, with the modernity of today's tech.
Amazing features include:
- 128x112 resolution (a whopping 0.014 megapixels!)
- 4 colors on screen at once
- A wide range of unique color palettes
- Contrast and Gamma (Brightness) controls
- Shooting timer
- Saving shot pictures to file

## Ownership / License
This piece of software is a fork of [webgbcam by Lana-chan](https://github.com/Lana-chan/webgbcam).
It exists for the purpose of making the overall experience of the app better, with bug fixes, UX revisions, and added features.
If you have any questions regarding why and how the app was created in the first place, you should take a look at the README on the original repo.

Below is the license of the original code, written by the original author of this program, which I hold no rights upon.
```
/*
 * ------------------------------------------------------------
 * "THE BEERWARE LICENSE" (Revision 42):
 * maple "mavica" syrup <maple@maple.pet> wrote this code.
 * As long as you retain this notice, you can do whatever you
 * want with this stuff. If we meet someday, and you think this
 * stuff is worth it, you can buy me a beer in return.
 * ------------------------------------------------------------
 */
```